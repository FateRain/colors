<h1 align="center">colors</h1>

<p align="center">
  <img alt="" src="https://img.shields.io/badge/release-v1.0.0-brightgreen" style="display: inline-block;" />
  <img alt="" src="https://img.shields.io/badge/build-pass-brightgreen" style="display: inline-block;" />
  <img alt="" src="https://img.shields.io/badge/cj-v0.53.13-brightgreen" style="display: inline-block;" />
  <img alt="" src="https://img.shields.io/badge/cjcov-99%25-brightgreen" style="display: inline-block;" />
  <img alt="" src="https://img.shields.io/badge/project-open-brightgreen" style="display: inline-block;" />
</p>

🚀 命令行着色，可以设置常用的文字样式、文字颜色预设、背景颜色预设。

![colors](./docs/colors.png)

## 安装

### 方式一: 本地

在`cjpm.toml`配置文件中新增配置如下

- path: 源码路径文件夹，把该仓库源码拉取到本地后填写。

```toml
[dependencies]
  [dependencies.colors]
    version = "1.0.0"
    path = "/xx/colors"
```

### 方式二: git

配置git仓库源码地址, 执行`cjpm update`命令, 包管理将从源仓库拉取依赖源码。

- git: 

```toml
[dependencies]
  [dependencies.colors]
    branch = "v1.0.0"
    git = "https://gitcode.com/FateRain/colors.git"
```

上述配置表示从git仓库中拉取`v1.0.0`分支版本的依赖。

## 使用

```cj
import colors.*

func main() {
  println(underline("underline"))
  println(red("red"))

  // 支持嵌套
  println(red("hello ${bgRed("changjie")}(仓颉)"))
}
```

